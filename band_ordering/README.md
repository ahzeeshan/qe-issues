How to reproduce:

-Run scf-pbesoc-kpt10.in using pw.x

-Run bands-pbesoc-kpt10.in using pw.x which computes kpts along path M-R-M. The CBM lies close to R point.

-Run bands-pbesoc-kpt10-MRM.x.in using bands.x

-Run python plot-band.py to get the image band-MRM.pdf
