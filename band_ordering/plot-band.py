import numpy as np
import matplotlib.pyplot as plt

d = np.loadtxt('scf-pbesoc-kpt10-MRM.dat.gnu')
nbnd = 60
d = d.reshape(60, 101, 2)

fig, ax = plt.subplots(1,1)
ax.plot(d[50, :, 0], d[50, :, 1])
ax.plot(d[51, :, 0], d[51, :, 1])

fig.savefig('band-MRM.pdf')


